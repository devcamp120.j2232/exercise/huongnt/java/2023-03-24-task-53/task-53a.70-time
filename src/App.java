public class App {
    public static void main(String[] args) throws Exception {
       // khởi tạo đối tượng date có tham số
       Time time1 = new Time(12, 01, 20);
       Time time2 = new Time(5, 12, 59);

       System.out.println("Time 1");
       System.out.println(time1.toString());
       System.out.println("Time 2");
       System.out.println(time1.toString());

       time1.nextSecond();
       System.out.println("Time 1- Next Second");
       System.out.println(time1.toString());

       time2.nextSecond();
       System.out.println("Time 2- Next Second");
       System.out.println(time2.toString());


    }
}
