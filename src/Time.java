public class Time {
    // khai báo thuộc tính
    public int hour;
    public int minute;
    public int second;

    // khởi tạo phương thức
    public Time(int hour, int minute, int second) {
        this.hour = hour;
        this.minute = minute;
        this.second = second;
    }

    // getter and setter
    public int getHour() {
        return hour;
    }

    public void setHour(int hour) {
        // Neu hour > 23 thi reset hour =0
        if (hour >= 0 && hour < 24) {
            this.hour = hour;
        } else {
            this.hour = 0;
        }
    }

    public int getMinute() {
        return minute;
    }

    public void setMinute(int minute) {
        // Neu minute > 60 thi reset minute = 0
        if (minute >= 0 && minute < 60) {
            this.minute = minute;
        } else {
            this.minute = 0;
        }
    }

    public int getSecond() {
        return second;
    }

    public void setSecond(int second) {
        // Neu secord > 60 thi reset second = 0
        if (second >= 0 && second < 60) {
            this.second = second;
        } else {
            this.second = 0;
        }
    }

    // phương thực tạo thêm
    public void setTime(int hour, int minute, int second) {
        this.hour = hour;
        this.minute = minute;
        this.second = second;
    }

    public void nextSecond() {
        this.setSecond(this.second + 1);
        // Neu giay tang len 60 thi giay reset ve 0, va tang phut len 1
        if (this.second == 0) {
            nextMinute();
        }
    }

    // Tang phut len 1 don vi
    public void nextMinute() {
        this.setMinute(this.minute + 1);
        // Neu phut tang len 60 thi phut reset ve 0, va tang gio len 1
        if (this.minute == 0) {
            nextHour();
        }
    }

    // Tang gio len 1 don vi, neu gio len 24 thi gio reset ve 0
    public void nextHour() {
        this.setHour(this.hour + 1);
    }

    // in ra console log
    @Override
    public String toString() {
        String str = "";
        if (hour == 12 || hour == 0) {
            str += 12;
        } else {
            str += (hour % 12);
        }
        str += ":";
        if (minute < 10) {
            str += "0";
        } else {
            str += "";
        }
        str += minute + ":";
        if (second < 10) {
            str += "0";
        } else {
            str += "";
        }
        str += second;
        if (hour < 12) {
            str += " AM";
        } else {
            str += " PM";
        }
        return str;
    }
}
